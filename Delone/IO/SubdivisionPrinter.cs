﻿using Delone.Geometry;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.IO
{
    public class SubdivisionPrinter
    {
        public static void PrintSubdivision(string filename, Subdivision subdiv)
        {
            using (var output = new StreamWriter(filename))
            {
                foreach (Point3D point in subdiv.Sites.OrderBy(p => p.Number))
                {
                    PrintPoint(point, output, false);
                }

                Console.WriteLine("FindTriangles");
                var triangles = subdiv.Edges().Select(
                    e => new Triangle2D(e.Org, e.Lnext.Org, e.Lnext.Lnext.Org))
                    .OrderBy(t => t.A).ThenBy(t => t.B).ThenBy(t => t.C).Distinct().ToList();
                Console.WriteLine("Triangles");
                foreach (Triangle2D t in triangles)
                {
                    PrintTriangle2D(t, output);
                }
            }
        }

        private static void PrintPoint(Point3D point, TextWriter textWriter, bool withNumber)
        {
            if (withNumber)
                textWriter.WriteLine($"{point.Number} v {point.X:0.##} {point.Y:0.##} {point.Z:0.##}");
            else
                textWriter.WriteLine($"v {point.X.ToString(CultureInfo.InvariantCulture)} " +
                    $"{point.Y.ToString(CultureInfo.InvariantCulture)} {point.Z.ToString(CultureInfo.InvariantCulture)}");
        }

        private static void PrintTriangle2D(Triangle2D triangle, TextWriter textWriter)
        {
            if (triangle.PointA.IsInfected || triangle.PointB.IsInfected || triangle.PointC.IsInfected)
                return;
            textWriter.WriteLine($"f {triangle.A} {triangle.B} {triangle.C}");
        }
    }
}
