﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Delone.Geometry;
using System.Globalization;

namespace Delone.IO
{
    public static class PointsReader
    {
        public static List<Point3D> ReadPoints(string filename, List<Tuple<int, int>> boundaryEdges,
            List<int> infectedPoints, CultureInfo cultureInfo)
        {
            List<Point3D> points = new List<Point3D>();
            
            using (StreamReader sr = new StreamReader(filename))
            {
                string verticesCount = sr.ReadLine();
                int n = int.Parse(verticesCount);
                string line;
                for (int i = 0; i < n; i++)
                {
                    line = sr.ReadLine();
                    string[] coords = line.Split(' ');
                    double x = double.Parse(coords[0], cultureInfo);
                    double y = double.Parse(coords[1], cultureInfo);
                    double z = double.Parse(coords[2], cultureInfo);
                    points.Add(new Point3D(x, y, z) { IsInputPoint = true });
                }
                string boundaryEdgesCount = sr.ReadLine();
                int m = int.Parse(boundaryEdgesCount);
                for (int i = 0; i < m; i++)
                {
                    line = sr.ReadLine();
                    string[] edgeEnds = line.Split(' ');
                    int org = int.Parse(edgeEnds[0]);
                    int dest = int.Parse(edgeEnds[1]);
                    boundaryEdges.Add(new Tuple<int, int>(org, dest));
                }
                string infNumString = sr.ReadLine();
                int infNum = int.Parse(infNumString);
                for (int i = 0; i < infNum; i++)
                {
                    string pointNum = sr.ReadLine();
                    int pNum = int.Parse(pointNum);
                    infectedPoints.Add(pNum);
                    points[pNum].IsInputPoint = false;
                }
            }

            return points;
        }
    }
}
