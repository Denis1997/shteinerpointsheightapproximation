﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Approximation
{
    public class Vector
    {
        private double[] values;

        public Vector(int dimension)
        {
            values = new double[dimension];
        }

        public Vector(double[] values)
        {
            this.values = values;
        }

        public double[] Values => values;

        public int Length => Values.Length;

        public double this[int x] => Values[x];

        public static Vector operator*(Vector v, double scalar)
        {
            double[] result = new double[v.Length];

            for (int i = 0; i < v.Length; i++)
            {
                result[i] = v.Values[i] * scalar;
            }

            return new Vector(result);
        }

        public static Vector operator+(Vector v1, Vector v2)
        {
            if (v1.Length != v2.Length)
            {
                throw new ArgumentException("Different vector lengths");
            }

            double[] result = new double[v1.Length];

            for (int i = 0; i < v1.Length; i++)
            {
                result[i] = v1.Values[i] + v2.Values[i];
            }

            return new Vector(result);
        }

        public static Matrix operator*(Vector v1, Vector v2)
        {
            if (v1.Length != v2.Length)
                throw new ArgumentException("Different lengthes");

            double[,] result = new double[v1.Length, v1.Length];
            for (int i = 0; i < v1.Length; i++)
                for (int j = 0; j < v2.Length; j++)
                {
                    result[i, j] = v1[i] * v2[j];
                }

            return new Matrix(result);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < Length; i++)
            {
                sb.Append(this[i]).Append(" ");
            }

            return sb.ToString();
        }
    }
}
