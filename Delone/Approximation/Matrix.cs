﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Approximation
{
    public class Matrix
    {
        private double[,] matrix;

        public Matrix(int dimension)
        {
            matrix = new double[dimension, dimension];
        }

        public Matrix(double[,] matrix)
        {
            this.matrix = matrix;
        }

        public int Dimension => matrix.GetLength(0);

        public double[,] MatrixValues => matrix;

        public double this[int x, int y] => MatrixValues[x, y];

        public static Matrix operator+(Matrix m1, Matrix m2)
        {
            if (m1.Dimension != m2.Dimension)
                throw new ArgumentException("Different dimensions");
            double[,] result = new double[m1.Dimension, m1.Dimension];
            for (int i = 0; i < m1.Dimension; i++)
                for (int j = 0; j < m2.Dimension; j++)
                {
                    result[i, j] = m1[i, j] + m2[i, j];
                }

            return new Matrix(result);
        }

        public static Matrix operator*(Matrix m, double scalar)
        {
            double[,] result = new double[m.Dimension, m.Dimension];
            for (int i = 0; i < m.Dimension; i++)
                for (int j = 0; j < m.Dimension; j++)
                {
                    result[i, j] = m[i, j] * scalar;
                }

            return new Matrix(result);
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < Dimension; i++)
            {
                for (int j = 0; j < Dimension; j++)
                {
                    sb.Append(MatrixValues[i, j]).Append("\t");
                }
                sb.Append("\n");
            }
            return sb.ToString();
        }
    }
}
