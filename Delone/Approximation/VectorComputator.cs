﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Approximation
{
    public class VectorComputator
    {
        private Func<double, double, double>[] basis;

        public VectorComputator(params Func<double, double, double>[] basis)
        {
            this.basis = basis;
        }

        public int Length => basis.Length;

        public Func<double, double, double> this[int x] => basis[x];

        public Vector Compute(double x, double y)
        {
            double[] result = new double[Length];
            for (int i = 0; i < Length; i++)
            {
                result[i] = basis[i](x, y);
            }

            return new Vector(result);
        }
    }
}
