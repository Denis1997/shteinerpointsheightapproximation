﻿using Delone.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone
{
    public static class Triangulator
    {
        public static List<Point3D> Triangulate(List<Point3D> points)
        {
            return points.OrderBy(p => p.X).ThenBy(p => p.Y).ToList();
        }
    }
}
