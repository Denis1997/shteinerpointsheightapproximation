﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Vector2D
    {
        public static readonly double Epsilon = 1e-6;
        public double X { get; set; }
        public double Y { get; set; }

        public Vector2D(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double Norm
            => Math.Sqrt(X * X + Y * Y);

        public void Normalize()
        {
            double len;
            if ((len = Norm) == 0.0)
                throw new DivideByZeroException();
            X /= len;
            Y /= len;
        }

        public static Vector2D operator+(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X + v2.X, v1.Y + v2.Y);
        }

        public static Vector2D operator -(Vector2D v1, Vector2D v2)
        {
            return new Vector2D(v1.X - v2.X, v1.Y - v2.Y);
        }

        public static double operator *(Vector2D v1, Vector2D v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }
    }
}
