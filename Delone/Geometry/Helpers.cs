﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public static class Helpers
    {
        public static readonly double Epsilon = 1e-6;

        public static Comparison<Point3D> ComparePointsByX = 
            (p1, p2) => {
                int comp = p1.X.CompareTo(p2.X);
                if (comp == 0)
                    return p1.Y.CompareTo(p2.Y);
                return comp;
            };

        public static double AngleFromFirstHorizontToSecond2D(this Point3D p1, Point3D p2)
        {
            if (Math.Abs(p1.X - p2.X) < 1e-6)
                if (p2.Y > p1.Y)
                    return Math.PI / 2;
                else
                    return 3 * Math.PI / 2;

            double tan = (p2.Y - p1.Y) / (p2.X - p1.X);
            double angle = Math.Atan(tan);
            if (p2.X > p1.X)
                return Math.PI - angle;
            int sign = tan == 0 ? -1 : Math.Sign(tan);
            return Math.PI * (1 + sign) - angle;
        }

        public static Edge MakeEdge()
        {
            QuadEdge quadEdge = new QuadEdge();
            return quadEdge.Edges[0];
        }

        /// <summary>
        /// This operator affects the two edge rings around the origins of a and b,
        /// and, independently, the two edge rings around the left faces of a and b.
        /// In each case, (i) if the two rings are distinct, Splice will combine
        /// them into one; (ii) if the two are the same ring, Splice will break it
        /// into two separate pieces.
        /// Thus, Splice can be used both to attach the two edges together, and
        /// to break them apart. See Guibas and Stolfi (1985) p.96 for more details
        /// and illustrations.
        /// </summary>
        public static void Splice(this Edge a, Edge b)
        {
            Edge alpha = a.Onext.Rot;
            Edge beta = b.Onext.Rot;
            Edge t1 = b.Onext;
            Edge t2 = a.Onext;
            Edge t3 = beta.Onext;
            Edge t4 = alpha.Onext;
            a.Next = t1;
            b.Next = t2;
            alpha.Next = t3;
            beta.Next = t4;
        }

        public static void DeleteEdge(this Edge e)
        {
            Splice(e, e.Oprev);
            Splice(e.Sym, e.Sym.Oprev);
            e.Org.OriginEdges.Remove(e);
            e.Sym.Org.OriginEdges.Remove(e.Sym);
        }

        /// <summary>
        /// Add a new edge e connecting the destination of a to the
        /// origin of b, in such a way that all three have the same
        /// left face after the connection is complete.
        /// Additionally, the data pointers of the new edge are set.
        /// </summary>
        public static Edge Connect(this Edge a, Edge b)
        {
            Edge e = MakeEdge();
            Splice(e, a.Lnext);
            Splice(e.Sym, b);
            e.EndPoints(a.Dest, b.Org);
            return e;
        }

        /// <summary>
        /// Essentially turns edge e counterclockwise inside its enclosing
        /// quadrilateral. The data pointers are modified accordingly.
        /// </summary>
        public static void Swap(this Edge e)
        {
            Edge a = e.Oprev;
            Edge b = e.Sym.Oprev;
            a.Org.OriginEdges.Remove(e);
            b.Org.OriginEdges.Remove(e.Sym);
            Splice(e, a);
            Splice(e.Sym, b);
            Splice(e, a.Lnext);
            Splice(e.Sym, b.Lnext);
            e.EndPoints(a.Dest, b.Dest);
        }

        /// <summary>
        /// Returns twice the area of the oriented triangle (a, b, c), i.e., the
        /// area is positive if the triangle is oriented counterclockwise.
        /// </summary>
        public static double TriArea(Point3D a, Point3D b, Point3D c)
        {
            return (b.X - a.X) * (c.Y - a.Y) - (b.Y - a.Y) * (c.X - a.X);
        }

        /// <summary>
        /// Returns true if the point d is inside the circle defined by the
        /// points a, b, c. See Guibas and Stolfi (1985) p.107.
        /// </summary>
        public static bool InCircle(Point3D a, Point3D b, Point3D c, Point3D d)
        {
            return (a.X * a.X + a.Y * a.Y) * TriArea(b, c, d) 
                - (b.X * b.X + b.Y * b.Y) * TriArea(a, c, d) 
                + (c.X * c.X + c.Y * c.Y) * TriArea(a, b, d) 
                - (d.X * d.X + d.Y * d.Y) * TriArea(a, b, c) > 0;
        }

        /// <summary>
        /// Returns true if the points a, b, c are in a counterclockwise order
        /// </summary>
        public static bool Ccw(Point3D a, Point3D b, Point3D c)
        {
            return TriArea(a, b, c) > 0;
        }

        public static bool RightOf(this Point3D x, Edge e)
        {
            return Ccw(x, e.Dest2d, e.Org2d);
        }

        public static bool LeftOf(this Point3D x, Edge e)
        {
            return Ccw(x, e.Org2d, e.Dest2d);
        }

        /// <summary>
        /// A predicate that determines if the point x is on the edge e
        /// The point is considered on if it is in the EPS-neighborhood
        /// of the edge.
        /// </summary>
        public static bool OnEdge(this Point3D x, Edge e)
        {
            double t1, t2, t3;
            t1 = (x - e.Org2d).Norm;
            t2 = (x - e.Dest2d).Norm;
            if (t1 < Epsilon || t2 < Epsilon)
                return true;
            t3 = (e.Org2d - e.Dest2d).Norm;
            if (t1 > t3 || t2 > t3)
                return false;
            Line line = new Line(e.Org2d, e.Dest2d);
            return Math.Abs(line.Eval(x)) < Epsilon;
        }

        public static double PointDist2D(this Point3D p1, Point3D p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }

        public static Vector3D ExtractPoint3D(this Point3D p1, Point3D p2)
        {
            return new Vector3D(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
        }

        public static double CountAngleP1P2P3(Point3D p1, Point3D p2, Point3D p3)
        {
            Vector3D v1 = p1.ExtractPoint3D(p2);
            Vector3D v2 = p3.ExtractPoint3D(p2);
            double cos = (v1 * v2) / v1.Norm / v2.Norm;
            return Math.Acos(cos);
        }

        public static double Count2DAngleP1P2P3(Point3D p1, Point3D p2, Point3D p3)
        {
            Vector2D v1 = p1 - p2;
            Vector2D v2 = p3 - p2;
            double cos = (v1 * v2) / v1.Norm / v2.Norm;
            return Math.Acos(cos);
        }
    }
}
