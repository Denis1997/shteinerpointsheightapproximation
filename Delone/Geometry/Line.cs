﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Line
    {
        public double A { get; }
        public double B { get; }
        public double C { get; }

        public Line(Point3D p, Point3D q)
        {
            Vector2D t = q - p;
            double len = t.Norm;
            A = t.Y / len;
            B = -t.X / len;
            C = -(A * p.X + B * p.Y);
        }

        public double Eval(Point3D p)
        {
            return A * p.X + B * p.Y + C;
        }
    }
}
