﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Triangle
    {
        public Point3D FirstPoint { get; set; }

        public Point3D SecondPoint { get; set; }

        public Point3D ThirdPoint { get; set; }

        public Triangle FirstTriangle { get; set; }

        public Triangle SecondTriangle { get; set; }

        public Triangle ThirdTriangle { get; set; }

        public Triangle() { }

        public Triangle(Point3D p1, Point3D p2)
        {
            if (p1 == null || p2 == null)
                throw new ArgumentNullException("one of the points is null");

            if (Helpers.ComparePointsByX(p1, p2) > 0)
            {
                FirstPoint = p1;
                SecondPoint = p2;
            }
            else
            {
                FirstPoint = p2;
                SecondPoint = p1;
            }

            FirstTriangle = SecondTriangle = ThirdTriangle = this;
        } 

        public Triangle(Point3D p1, Point3D p2, Point3D p3)
        {
            if (p1 == null || p2 == null || p3 == null)
                throw new ArgumentNullException("one of the points is null");

            Point3D[] vertices = new[] { p1, p2, p3 };
            Array.Sort(vertices, Helpers.ComparePointsByX);
            FirstPoint = vertices[0];
            if (FirstPoint.AngleFromFirstHorizontToSecond2D(vertices[1]) < 
                FirstPoint.AngleFromFirstHorizontToSecond2D(vertices[2]))
            {
                SecondPoint = vertices[1];
                ThirdPoint = vertices[2];
            }
            else
            {
                SecondPoint = vertices[2];
                ThirdPoint = vertices[1];
            }

            FirstTriangle = SecondTriangle = ThirdTriangle = this;
        }
    }
}
