﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Vector3D
    {
        public static readonly double Epsilon = 1e-6;
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Vector3D(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public double Norm
            => Math.Sqrt(X * X + Y * Y + Z*Z);

        public void Normalize()
        {
            double len;
            if ((len = Norm) == 0.0)
                throw new DivideByZeroException();
            X /= len;
            Y /= len;
            Z /= len;
        }

        public static double operator *(Vector3D v1, Vector3D v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }
    }
}
