﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Edge
    {
        internal int Num { get; set; }
        internal Edge Next { get; set; }
        public Point3D Data { get; set; }

        public bool IsVisited { get; set; }

        public QuadEdge Qedge { get; set; }

        public Edge Rot
            => Num < 3 ? Qedge.Edges[Num + 1] : Qedge.Edges[Num - 3];

        public Edge InvRot
            => Num > 0 ? Qedge.Edges[Num - 1] : Qedge.Edges[Num + 3];

        public Edge Sym 
            => Num < 2 ? Qedge.Edges[Num + 2] : Qedge.Edges[Num - 2];
        public Edge Onext
            => Next;
        public Edge Oprev
            => Rot.Onext.Rot;
        public Edge Dnext
            => Sym.Onext.Sym;
        public Edge Dprev
            => InvRot.Onext.InvRot;
        public Edge Lnext
            => InvRot.Onext.Rot;
        public Edge Lprev
            => Onext.Sym;
        public Edge Rnext
            => Rot.Onext.InvRot;
        public Edge Rprev
            => Sym.Onext;
        public Point3D Org
            => Data;
        public Point3D Dest
            => Sym.Data;
        public Point3D Org2d
            => Data;
        public Point3D Dest2d
            => Num < 2 ? Qedge.Edges[Num + 2].Data 
                       : Qedge.Edges[Num - 2].Data;

        public void EndPoints(Point3D orig, Point3D dest)
        {
            Data = orig;
            Sym.Data = dest;
            if (!orig.OriginEdges.Any(e => e.Dest.Number == dest.Number))
            {
                orig.OriginEdges.Add(this);
            }
            if (!dest.OriginEdges.Any(e => e.Dest.Number == orig.Number))
            {
                dest.OriginEdges.Add(Sym);
            }
        }
    }
}
