﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Triangle2D : IEquatable<Triangle2D>
    {
        public int A { get; set; }
        public int B { get; set; }
        public int C { get; set; }

        public Point3D PointA { get; set; }
        public Point3D PointB { get; set; }
        public Point3D PointC { get; set; }

        public Triangle2D(Point3D a, Point3D b, Point3D c)
        {
            PointA = a;
            PointB = b;
            PointC = c;
            int[] vertices = new int[] { a.Number, b.Number, c.Number };
            Array.Sort(vertices);
            A = vertices[0];
            B = vertices[1];
            C = vertices[2];
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(Triangle2D))
                return false;

            Triangle2D t = obj as Triangle2D;
            return A == t.A && B == t.B && C == t.C;
        }

        public bool Equals(Triangle2D other)
        {
            return Equals((object)other);
        }

        public override int GetHashCode()
        {
            return A ^ B ^ C;
        }
    }
}
