﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class QuadEdge
    {
        internal Edge[] Edges = new Edge[4];

        public QuadEdge()
        {
            Edges[0] = new Edge { Num = 0, Qedge = this };
            Edges[1] = new Edge { Num = 1, Qedge = this };
            Edges[2] = new Edge { Num = 2, Qedge = this };
            Edges[3] = new Edge { Num = 3, Qedge = this };
            Edges[0].Next = Edges[0];
            Edges[1].Next = Edges[3];
            Edges[2].Next = Edges[2];
            Edges[3].Next = Edges[1];
        }
    }
}
