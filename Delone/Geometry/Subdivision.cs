﻿using Delone.Approximation;
using Delone.IO;
using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Subdivision
    {
        [Flags]
        public enum TriangleType
        {
            GoodTriangle = 1, // don't need to refine
            BadTriangle = 2,  // can refine via inserting circumcenter
            LittleTriangle = 4, // too little to refine
            BadCircumcenter = 8, // circumcenter is out of the big triangle
            NotATriangle = 16, // maybe will be need if i will implement holes
        }

        private Point3D A { get; set; }
        private Point3D B { get; set; }
        private Point3D C { get; set; }

        private double LeftBoundary { get; set; }
        private double BottomBoundary { get; set; }
        private double RightBoundary { get; set; }
        private double UpBoundary { get; set; }
        private double BigTriangleAngle { get; set; }

        private int Number { get; set; }

        public List<Point3D> Sites { get; set; }

        public int InputPointsNumber { get; set; }

        /// <summary>
        /// Initialize a subdivision to the triangle defined by the points a, b, c.
        /// </summary>
        public Subdivision(Point3D a, Point3D b, Point3D c)
        {
            Sites = new List<Point3D>();
            Sites.Add(a);
            Sites.Add(b);
            Sites.Add(c);
            A = a;
            B = b;
            C = c;
            A.Number = Number = 1;
            Number = 2;
            B.Number = Number++;
            C.Number = Number++;
            LeftBoundary = Math.Min(A.X, Math.Min(B.X, C.X));
            BottomBoundary = Math.Min(A.Y, Math.Min(B.Y, C.Y));
            UpBoundary = Math.Max(A.Y, Math.Max(B.Y, C.Y));
            RightBoundary = Math.Max(A.X, Math.Max(B.X, C.X));
            BigTriangleAngle = (UpBoundary - BottomBoundary) / (RightBoundary - LeftBoundary);
            Edge ea = Helpers.MakeEdge();
            ea.EndPoints(A, B);
            Edge eb = Helpers.MakeEdge();
            ea.Sym.Splice(eb);
            eb.EndPoints(B, C);
            Edge ec = Helpers.MakeEdge();
            eb.Sym.Splice(ec);
            ec.EndPoints(C, A);
            ec.Sym.Splice(ea);
            StartingEdge = ea;
        }

        //public void 
        /// <summary>
        /// Inserts a new point into a subdivision representing a Delaunay
        /// triangulation, and fixes the affected edges so that the result
        /// is still a Delaunay triangulation. This is based on the
        /// pseudocode from Guibas and Stolfi (1985) p.120, with slight
        /// modifications and a bug fix.
        /// </summary>
        public Point3D InsertSite(Point3D x, Edge startingEdge = null, bool checkDelone = true)
        {
            Edge e;
            if (ReferenceEquals(startingEdge, null))
                e = Locate(x);
            else
                e = Locate(x, startingEdge);
            if (x == e.Dest) // point is already in
            {
                return e.Dest;
            }
            if (x == e.Org) // point is already in
            {
                return e.Org;
            }
            else if (x.OnEdge(e))
            {
                e = e.Oprev;
                e.Onext.DeleteEdge();
            }
            x.Number = Number++;
            Sites.Add(x);
            // Connect the new point to the vertices of the containing
            // triangle (or quadrilateral, if the new point fell on an
            // existing edge.)
            Edge baseEdge = Helpers.MakeEdge();
            baseEdge.EndPoints(e.Org, x);
            baseEdge.Splice(e);
            StartingEdge = baseEdge;
            do
            {
                baseEdge = e.Connect(baseEdge.Sym);
                e = baseEdge.Oprev;
            } while (e.Lnext != StartingEdge);

            if (!checkDelone)
            {
                //SubdivisionPrinter.PrintSubdivision("NewPointInserted.obj", this);
                //Console.WriteLine("Printed");
                //Console.ReadKey();
                return x;
            }
            // Examine suspect edges to ensure that the Delaunay condition
            // is satisfied.
            do
            {
                Edge t = e.Oprev;
                if (t.Dest2d.RightOf(e) && Helpers.InCircle(e.Org2d, t.Dest2d, e.Dest2d, x))
                {
                    e.Swap();
                    e = baseEdge.Oprev;
                }
                else if (e.Onext == StartingEdge) // no more suspect edges
                {
                    //SubdivisionPrinter.PrintSubdivision("NewPointInserted.obj", this);
                    //Console.WriteLine("Printed");
                    //Console.ReadKey();
                    return x;
                }
                else // pop a suspect edge
                    e = e.Onext.Lprev;
            } while (true);
        }

        public void Constraine()
        {
            bool isBoundaryBroken = true;
            while (isBoundaryBroken)
            //for (int j = 0; j < 4; j++)
            {
                isBoundaryBroken = false;
                for (int i = 0; i < Sites.Count; i++)
                {
                    while (IsInvalidBoundary(Sites[i]))
                    //if (Sites[i].IsBoundaryVertice)
                    {
                        isBoundaryBroken = true;
                        InsertBoundaryEdgeMiddle(Sites[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Moving Least Squares
        /// </summary>
        public void ApproximateWithMLS(Func<double, double> teta, VectorComputator vector)
        {
            foreach (var site in Sites.Where(s => !s.IsInfected && !s.IsInputPoint && !s.IsBoundaryVertice))
            {
                Matrix A = new Matrix(vector.Length);
                Vector b = new Vector(vector.Length);

                for (int i = 0; i < InputPointsNumber; i++)
                {
                    if (!Sites[i].IsInputPoint)
                        continue;
                    double weight = teta((site - Sites[i]).Norm);
                    //Console.WriteLine(weight);
                    //Console.ReadKey();
                    Vector vi = vector.Compute(Sites[i].X, Sites[i].Y);
                    A += vi * (vi * weight);
                    b += vi * (Sites[i].Z * weight);
                }

                var Amatr = Matrix<double>.Build.DenseOfArray(A.MatrixValues);
                var bVector = Vector<double>.Build.Dense(b.Values);
                var coeffs = Amatr.Solve(bVector);

                double height = 0;                
                for (int i = 0; i < coeffs.Count; i++)
                    height += coeffs[i] * vector[i](site.X, site.Y);
                //Console.WriteLine($"Height: {height}");
                //Console.WriteLine($"X: {site.X}\nY:{site.Y}\nCoeffs: ");
                //for (int i = 0; i < coeffs.Count; i++)
                //    Console.Write($"{coeffs[i]} ");
                //Console.WriteLine();
                //Console.WriteLine("Matrix A:\n" + A);
                //Console.WriteLine("Vector b:\n" + b);
                //Console.ReadKey();
                site.Z = height;
            }

        }

        public void ApproximateInsertedByLocals(
            Action<Point3D> approximator, int numberOfIterations, Func<double, double, bool> isSeriouslyUpdated)
        {
            bool isUpdated = true;
            for (int i = 0; i < numberOfIterations; i++)
            {
                //Console.WriteLine(i);
                if (!isUpdated)
                    break;
                foreach (Point3D p in Sites)
                {
                    if (!p.IsInputPoint && !p.IsInfected && !p.IsBoundaryVertice)
                    {
                        //bool localUpdated;
                        //int counter = 0;
                        //do
                        //{
                        //localUpdated = false;
                        double oldH = p.Z;
                        approximator(p);
                        double newH = p.Z;
                        if (isSeriouslyUpdated(oldH, newH))
                        {
                            //localUpdated = true;
                            isUpdated = true;
                            p.SeriousUpdatesCount++;
                        }
                        //} while (localUpdated && counter++ < 10);
                    }
                }
            }
        }

        private bool IsInvalidBoundary(Point3D p)
        {
            if (!p.IsBoundaryVertice)
                return false;
            foreach (Edge e in p.OriginEdges)
            {
                if (e.Dest == p.NextOnBoundary)
                    return false;
            }
            return true;
        }

        private void InsertBoundaryEdgeMiddle(Point3D p)
        {
            Point3D dest = p.NextOnBoundary;
            Point3D middlePoint = new Point3D((p.X + dest.X) / 2, (p.Y + dest.Y) / 2, (p.Z + dest.Z) / 2);
            //Console.WriteLine($"From {p.Number} to: {p.NextOnBoundary.Number}");
            //foreach (Edge e in p.OriginEdges)
            //    Console.WriteLine($"\t{e.Dest.Number}");
            middlePoint = InsertSite(middlePoint);
            middlePoint.IsBoundaryVertice = true;
            //Console.WriteLine($"{middlePoint.X} {middlePoint.Y}: from {p.Number} to {dest.Number} middle: {middlePoint.Number}");
            if (ReferenceEquals(middlePoint, dest))
            {
                Console.WriteLine("IT'S ALREADY A DESTINATION!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
                Console.ReadKey();
                return;
            }
            if (ReferenceEquals(middlePoint, p))
            {
                Console.WriteLine("IT'S I AM!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
                Console.ReadKey();
                return;
            }
            middlePoint.NextOnBoundary = p.NextOnBoundary;
            p.NextOnBoundary = middlePoint;
        }

        /// <summary>
        /// Enumerates all undirected edges of subdivision
        /// </summary>
        public IEnumerable<Edge> Edges()
        {
            for (int i = 0; i < Sites.Count; i++)
                for (int j = 0; j < Sites[i].OriginEdges.Count; j++)
                {
                    yield return Sites[i].OriginEdges[j];
                }
            yield break;
        }

        public double CountTotalGaussianCurvature(Func<Point3D, double> curvatureCounter = null)
        {
            if (ReferenceEquals(curvatureCounter, null))
            {
                curvatureCounter = CountGaussianCurvature;
            }
            double result = Sites.Where(p => !p.IsInfected).Sum(p => curvatureCounter(p));

            return result;
        }

        public static double CountGaussianCurvature(Point3D p)
        {
            double teta = 0;
            double area = 0;
            double totalAngle = 0;
            Edge firstEdge = p.OriginEdges[0];
            Edge curEdge = firstEdge;
            do
            {
                Edge next = curEdge.Onext;
                if (next.Dest.IsInfected || curEdge.Dest.IsInfected)
                {
                    curEdge = curEdge.Onext;
                    continue;
                }
                double anglePart = Helpers.CountAngleP1P2P3(curEdge.Dest, p, next.Dest);
                teta += anglePart;
                area += (curEdge.Dest.ExtractPoint3D(p).Norm * next.Dest.ExtractPoint3D(p).Norm) * Math.Sin(anglePart) / 2.0;
                totalAngle += Helpers.Count2DAngleP1P2P3(curEdge.Dest, p, next.Dest);
                curEdge = curEdge.Onext;
            } while (firstEdge != curEdge);

            if (teta > totalAngle)
            {
                //Console.WriteLine($"teta: {teta}");
                teta = totalAngle;
            }

            double curvature = (totalAngle - teta) / area;
            return curvature;
        }

        public void RefineMesh(double minAngle, double minSquare, double maxSquare)
        {
            bool badTriangleFounded;
            minAngle = Math.PI / 180 * minAngle;
            int limit = 100;
            int iter = 0;
            do
            {
                iter++;
                if (iter > limit)
                    break;
                badTriangleFounded = false;
                foreach (Edge edge in Edges())
                {
                    switch (GetTriangleType(edge, minAngle, minSquare, maxSquare))
                    {
                        case TriangleType.GoodTriangle:
                        case TriangleType.LittleTriangle:
                        case TriangleType.NotATriangle:
                            continue;
                        case TriangleType.BadCircumcenter:
                            badTriangleFounded = true;
                            //Console.WriteLine($"Bad circumcenter {edge.Org.Number} {edge.Dest.Number} {edge.Lnext.Dest.Number}");
                            //Console.WriteLine($"Bad circumcenter {edge.Org.Z} {edge.Dest.Z} {edge.Lnext.Dest.Z}");
                            Point3D edgeMiddle = InsertBadCircumcenter(edge);
                            //Console.WriteLine("Inserted");
                            break;
                        case TriangleType.BadTriangle:
                            badTriangleFounded = true;
                            //Console.WriteLine($"Bad triangle {edge.Org.Number} {edge.Dest.Number} {edge.Lnext.Dest.Number}");
                            Point3D circumcenter = InsertCircumcenter(edge);
                            //Console.WriteLine($"Inserted point number: {circumcenter.Number}");
                            //Console.WriteLine($"{circumcenter.X} {circumcenter.Y}");
                            //Console.WriteLine("Inserted");
                            break;
                    }
                }
                //Console.WriteLine("bad");
            } while (badTriangleFounded);
        }

        public bool IsFalsificatedBoundaryEdge(Edge e)
        {
            Point3D org = e.Org;
            Point3D dest = e.Dest;
            if (!(org.IsBoundaryVertice && dest.IsBoundaryVertice))
                return false;
            if (org.NextOnBoundary.Number != dest.Number && dest.NextOnBoundary.Number != org.Number)
                return true;
            return false;
        }

        public void CheckFalsificatedBoundaryEdges()
        {
            for (int i = 0; i < Sites.Count; i++)
            {
                for (int j = 0; j < Sites[i].OriginEdges.Count; j++)
                {
                    Edge e = Sites[i].OriginEdges[j];
                    if (IsFalsificatedBoundaryEdge(e))
                    {
                        Point3D middlePoint = new Point3D((e.Org.X + e.Dest.X) / 2, (e.Org.Y + e.Dest.Y) / 2);
                        InsertSite(middlePoint, e, false);
                    }
                }
            }
        }

        public void InfectPoint(Point3D p)
        {
            p.IsInfected = true;
            Stack<Point3D> infectedPoints = new Stack<Point3D>();
            infectedPoints.Push(p);
            while (infectedPoints.Count != 0)
            {
                Point3D currentPoint = infectedPoints.Pop();
                //Console.WriteLine($"From: {currentPoint.Number} Count: {currentPoint.OriginEdges.Count}");
                foreach (Edge edge in currentPoint.OriginEdges)
                {
                    if (!edge.Dest.IsInfected && !edge.Dest.IsBoundaryVertice)
                    {
                        //Console.WriteLine($"\tInfect {edge.Dest.Number}");
                        edge.Dest.IsInfected = true;
                        infectedPoints.Push(edge.Dest);
                    }
                    //else
                    //    Console.WriteLine($"\tNot infect {edge.Dest.Number}");
                }
                //SubdivisionPrinter.PrintSubdivision("InfectedStep.obj", this);
                //Console.WriteLine("Infected step printed");
                //Console.ReadKey();
            }
        }
        public Edge StartingEdge { get; set; }

        private Point3D InsertBadCircumcenter(Edge edge)
        {
            Point3D a, b, c;
            a = edge.Org;
            b = edge.Lnext.Org;
            c = edge.Lnext.Lnext.Org;
            Point3D circumcenter = CalculateCircumcenter(a, b, c);
            Edge curEdge = edge;
            Point3D inserted;
            while (!circumcenter.RightOf(curEdge))
            {
                //Console.WriteLine(curEdge.Org.Number + " " +  curEdge.Dest.Number);
                curEdge = curEdge.Lnext;
                if (ReferenceEquals(curEdge, edge)) // may be curcumcenter is very close to an edge
                {
                    inserted = InsertSite(circumcenter, curEdge);
                    return inserted;
                }
            }
            //Console.WriteLine($"{a.Z} {b.Z} {c.Z}");
            Point3D edgeMiddle = new Point3D((curEdge.Org.X + curEdge.Dest.X) / 2, (curEdge.Org.Y + curEdge.Dest.Y) / 2, (curEdge.Org.Z + curEdge.Dest.Z) / 2);
            //Console.WriteLine($"{edgeMiddle.Z}");
            inserted = InsertSite(edgeMiddle, curEdge);
            return inserted;
        }

        private Point3D InsertCircumcenter(Edge edge)
        {
            Point3D a, b, c;
            a = edge.Org;
            b = edge.Lnext.Org;
            c = edge.Lnext.Lnext.Org;
            Point3D circumcenter = CalculateCircumcenter(a, b, c);
            InsertSite(circumcenter, edge);
            return circumcenter;
        }

        public TriangleType GetTriangleType(Edge edge, double minAngle, double minSquare, double maxArea)
        {
            Point3D a, b, c;
            a = edge.Org;
            b = edge.Lnext.Org;
            c = edge.Lnext.Lnext.Org;
            if (IsOneOfGreatBoundaryVertice(a) || IsOneOfGreatBoundaryVertice(b) || IsOneOfGreatBoundaryVertice(c))
                return TriangleType.GoodTriangle;
            bool isBadTriangle = false;
            double area = CountArea(a, b, c);
            if (area <= minSquare)
                return TriangleType.GoodTriangle;
            if (area > maxArea)
                isBadTriangle = true;
            else
            {
                double minTriangleAngle = FindMinimalAngle(a, b, c);
                if (minTriangleAngle < minAngle)
                    isBadTriangle = true;
            }

            if (!isBadTriangle)
                return TriangleType.GoodTriangle;

            Point3D circumcenter = CalculateCircumcenter(a, b, c);
            if (IsOutOfBigTriangle(circumcenter))
                return TriangleType.BadCircumcenter;
            Edge locEdge = Locate(circumcenter, edge);
            Point3D na, nb, nc;
            na = locEdge.Org;
            nb = locEdge.Lnext.Org;
            nc = locEdge.Lnext.Lnext.Org;
            if (IsOneOfGreatBoundaryVertice(na) || IsOneOfGreatBoundaryVertice(nb) || IsOneOfGreatBoundaryVertice(nc))
                return TriangleType.BadCircumcenter;
            return TriangleType.BadTriangle;
        }

        private bool IsOneOfGreatBoundaryVertice(Point3D p)
        {
            return p.Equals(A) || p.Equals(B) || p.Equals(C);
        }

        private bool IsOutOfBigTriangle(Point3D p)
        {
            if (p.X < LeftBoundary || p.Y < BottomBoundary || p.X > RightBoundary || p.Y > UpBoundary)
                return true;
            double angle = (p.Y - BottomBoundary) / (RightBoundary - p.X);
            if (angle >= BigTriangleAngle)
                return true;
            return false;
        }

        public Point3D CalculateCircumcenter(Point3D p1, Point3D p2, Point3D p3)
        {
            double a = p2.X - p1.X;
            double b = p2.Y - p1.Y;
            double c = p3.X - p1.X;
            double d = p3.Y - p1.Y;
            double e = a * (p1.X + p2.X) + b * (p1.Y + p2.Y);
            double f = c * (p1.X + p3.X) + d * (p1.Y + p3.Y);
            double g = 2.0 * (a * (p3.Y - p2.Y) - b * (p3.X - p2.X));
            if (Math.Abs(g) < Helpers.Epsilon)
                return null;
            double x = (d * e - b * f) / g;
            double y = (a * f - c * e) / g;
            return new Point3D(x, y, Plane(p1, p2, p3)(x, y));
        }

        private double FindMinimalAngle(Point3D a, Point3D b, Point3D c)
        {
            double aside = (a - b).Norm;
            double bside = (a - c).Norm;
            double cside = (b - c).Norm;
            double aangle = Math.Acos((bside * bside + cside * cside - aside * aside) / (2 * bside * cside));
            double bangle = Math.Acos(aside * aside + cside * cside - bside * bside) / (2 * aside * cside);
            double cangle = Math.PI - aangle - bangle;
            double minAngle = Math.Min(aangle, Math.Min(bangle, cangle));
            return minAngle;
        }

        private double CountArea(Point3D a, Point3D b, Point3D c)
        {
            return Math.Abs(a.X * (b.Y - c.Y) + b.X * (c.Y - a.Y) + c.X * (a.Y - b.Y)) / 2;
        }

        /// <summary>
        /// Returns an edge e, s.t. either x is on e, or e is an edge of
        /// a triangle containing x. The search starts from startingEdge
        /// and proceeds in the general direction of x. Based on the
        /// pseudocode in Guibas and Stolfi (1985) p.121.
        /// </summary>
        private Edge Locate(Point3D x)
        {
            Edge e = StartingEdge;
            return Locate(x, e);
        }

        /// <summary>
        /// Returns an edge e, s.t. either x is on e, or e is an edge of
        /// a triangle containing x. The search starts from specified edge
        /// and proceeds in the general direction of x. Based on the
        /// pseudocode in Guibas and Stolfi (1985) p.121.
        /// </summary>
        private Edge Locate(Point3D x, Edge e)
        {
            while (true)
            {
                if (x == e.Org2d || x == e.Dest2d)
                    return e;
                if (x.RightOf(e))
                    e = e.Sym;
                else if (!x.RightOf(e.Onext))
                    e = e.Onext;
                else if (!x.RightOf(e.Dprev))
                    e = e.Dprev;
                else
                    return e;
            }
        }

        public Func<double, double, double> Plane(Point3D p1, Point3D p2, Point3D p3)
        {
            double x1 = p1.X, x2 = p2.X, x3 = p3.X;
            double y1 = p1.Y, y2 = p2.Y, y3 = p3.Y;
            double z1 = p1.Z, z2 = p2.Z, z3 = p3.Z;

            return (x, y) =>
            {
                return -((x - x1) * ((y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1)) +
                                (y - y1) * ((z2 - z1) * (x3 - x1) - (x2 - x1) * (z3 - z1)))
                        / ((x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1))
                        + z1;
            };
        }
    }
}
