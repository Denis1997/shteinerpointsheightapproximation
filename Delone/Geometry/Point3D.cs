﻿using MIConvexHull;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delone.Geometry
{
    public class Point3D : IVertex
    {
        public Point3D(double x, double y, double z = 0)
        {
            Position = new double[] { x, y, z };
            OriginEdges = new List<Edge>();
        }

        public Point3D(double x, double y, int number)
            :this(x, y)
        {
            Number = number;
        }

        public int Number { get; set; }
        public bool IsBoundaryVertice { get; set; }
        public bool IsInputPoint { get; set; }
        public bool IsInfected { get; set; }
        public int SeriousUpdatesCount { get; set; }
        public Point3D NextOnBoundary { get; set; }
        public List<Edge> OriginEdges { get; set; }

        public double X
        {
            get
            {
                return Position[0];
            }
            set
            {
                Position[0] = value;
            }
        }

        public double Y
        {
            get
            {
                return Position[1];
            }
            set
            {
                Position[1] = value;
            }
        }

        public double Z
        {
            get
            {
                return Position[2];
            }
            set
            {
                Position[2] = value;
            }
        }

        /// <summary>
        /// X, Y and Z position as array
        /// </summary>
        public double[] Position { get; }

        public static Point3D operator +(Point3D a, Vector2D v)
        {
            return new Point3D(a.X + v.X, a.Y + v.Y);
        }

        public static Vector2D operator -(Point3D a, Point3D b)
        {
            return new Vector2D(a.X - b.X, a.Y - b.Y);
        }

        public static bool operator ==(Point3D p1, Point3D p2)
        {
            return (p1 - p2).Norm < Helpers.Epsilon;
        }

        public static bool operator !=(Point3D p1, Point3D p2)
        {
            return !(p1 == p2);
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != typeof(Point3D))
                return false;
            Point3D other = obj as Point3D;
            return this == other;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }
    }
}
