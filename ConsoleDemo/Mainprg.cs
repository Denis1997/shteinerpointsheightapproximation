﻿using Delone;
using Delone.Geometry;
using Delone.IO;
using Delone.Approximation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleDemo
{
    class Mainprg
    {
        static void Main(string[] args)
        {
            new Mainprg().SolveProblem(args);
        }

        public void SolveProblem(string[] args)
        {
            if (args.Length != 4)
            {
                Console.WriteLine("(filename) (minAngle grads) (minSquare) (maxSquare)");
                return;
            }

            List<Tuple<int, int>> boundaryEdges = new List<Tuple<int, int>>();
            List<int> infectedPoints = new List<int>();
            List<Point3D> points = PointsReader.ReadPoints(args[0], boundaryEdges, infectedPoints, CultureInfo.InvariantCulture);

            double minAngle = double.Parse(args[1], CultureInfo.InvariantCulture);
            double minSquare = double.Parse(args[2], CultureInfo.InvariantCulture);
            double maxSquare = double.Parse(args[3], CultureInfo.InvariantCulture);
            TextWriter output = Console.Out;
            
            foreach (var edge in boundaryEdges)
            {
                points[edge.Item1].IsBoundaryVertice = true;
                points[edge.Item1].NextOnBoundary = points[edge.Item2];
            }

            double left, up, right, down;
            left = up = right = down = 0;
            foreach (Point3D p in points)
            {
                if (p.X < left)
                    left = p.X;
                if (p.X > right)
                    right = p.X;
                if (p.Y < down)
                    down = p.Y;
                if (p.Y > up)
                    up = p.Y;
            }
            left--;
            down--;
            right++;
            up++;
            
            Point3D a = new Point3D(left, down); //left down corner of a big triangle
            Point3D b = new Point3D(left, up + up - down); // left top corner of a big triangle
            Point3D c = new Point3D(right + right - left, down); // right down corner of a big triangle

            Stopwatch sw = Stopwatch.StartNew();
            Subdivision subdiv = new Subdivision(a, b, c);
            foreach (Point3D point in points)
            {
                subdiv.InsertSite(point);
            }
            sw.Stop();

            subdiv.InputPointsNumber = subdiv.Sites.Count;
            Console.WriteLine($"Base delone builded in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            SubdivisionPrinter.PrintSubdivision("output.obj", subdiv);
            sw.Stop();
            Console.WriteLine($"Base delone printed to file in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            subdiv.Constraine();
            sw.Stop();
            Console.WriteLine($"Constrained delone builded in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            SubdivisionPrinter.PrintSubdivision("constrained.obj", subdiv);
            sw.Stop();
            Console.WriteLine($"Constrained delone printed in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            subdiv.RefineMesh(minAngle, minSquare, maxSquare);
            sw.Stop();
            Console.WriteLine($"Refined delone builded in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            SubdivisionPrinter.PrintSubdivision("quality.obj", subdiv);
            sw.Stop();
            Console.WriteLine($"Refined delone printed in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            subdiv.Constraine();
            Console.WriteLine("!!!constrained");
            Console.ReadKey(false);
            //subdiv.RefineMesh(minAngle, minSquare, maxSquare);
            //Console.WriteLine("!!Refined");
            //subdiv.Constraine();
            //Console.WriteLine("!!!constrained again");
            sw.Stop();
            Console.WriteLine($"Constrained refined delone builded in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            SubdivisionPrinter.PrintSubdivision("constrainedQuality.obj", subdiv);
            sw.Stop();
            Console.WriteLine($"Constrained refined delone printed in {sw.ElapsedMilliseconds / 1000.0}s");

            sw = Stopwatch.StartNew();
            subdiv.InfectPoint(subdiv.Sites[0]);
            foreach (int pNum in infectedPoints)
            {
                subdiv.InfectPoint(points[pNum]);
            }
            sw.Stop();
            Console.WriteLine($"Constrained refined delone infected in {sw.ElapsedMilliseconds / 1000.0}s");

            //subdiv.InfectOutBoundaries();
            sw = Stopwatch.StartNew();
            SubdivisionPrinter.PrintSubdivision("infectedConstrainedQuality.obj", subdiv);
            sw.Stop();
            Console.WriteLine($"Constrained refined infected delone printed in {sw.ElapsedMilliseconds / 1000.0}s");

            double curvature = subdiv.CountTotalGaussianCurvature();
            Console.WriteLine($"CURVATURE: {curvature}");
            double curvatureOnBoundary = subdiv.Sites.Where(s => s.IsBoundaryVertice)
                .Sum(s => Subdivision.CountGaussianCurvature(s));
            Console.WriteLine($"CURVATURE ON BOUNDARY: {curvatureOnBoundary}");
            //todo
            //ApproximateWithCycleAlgorithm(subdiv);
            ApproximateWithMLS(subdiv);
            curvature = subdiv.CountTotalGaussianCurvature();
            Console.WriteLine($"CURVATURE: {curvature}");
            curvatureOnBoundary = subdiv.Sites.Where(s => s.IsBoundaryVertice)
                .Sum(s => Subdivision.CountGaussianCurvature(s));
            Console.WriteLine($"CURVATURE ON BOUNDARY: {curvatureOnBoundary}");
            SubdivisionPrinter.PrintSubdivision("approximated.obj", subdiv);
        }

        public double Epsilon { get; set; } = 1e-6;

        public void ApproximateWithMLS(Subdivision subdiv)
        {
            Stopwatch sw = Stopwatch.StartNew();
            subdiv.ApproximateWithMLS(x => 1 / Math.Sqrt(x + 2), new VectorComputator(
                (x, y) => 1,
                (x, y) => x,
                (x, y) => y,
                (x, y) => x * y,
                (x, y) => x * x,
                (x, y) => y * y));

            sw.Stop();
            Console.WriteLine($"Result heights approximated with MLS in {sw.ElapsedMilliseconds / 1000.0}s");
        }

        public void ApproximateWithCycleAlgorithm(Subdivision subdiv)
        {
            Stopwatch sw = Stopwatch.StartNew();
            subdiv.ApproximateInsertedByLocals(PointWeightedAverageApproximator, 1000, (oldH, newH) =>
            {
                if (Math.Abs(oldH - newH) > Epsilon)
                {
                    return true;
                }
                return false;
            });

            sw.Stop();
            Console.WriteLine($"Result heights approximated in {sw.ElapsedMilliseconds / 1000.0}s");
            double averageUpdates =
                subdiv.Sites.Where(v => !v.IsInputPoint && !v.IsInfected).Average(p => p.SeriousUpdatesCount);
            int maxUpdated = subdiv.Sites.Max(p => p.SeriousUpdatesCount);
            int minUpdated = subdiv.Sites.Min(p => p.SeriousUpdatesCount);
            Console.WriteLine($"Maximum updated: {maxUpdated}");
            Console.WriteLine($"Average updated count: {averageUpdates}");
            Console.WriteLine($"Minimum updated: {minUpdated}");
        }

        public void PointWeightedAverageApproximator(Point3D p)
        {
            IEnumerable<Edge> influenceEdges = p.OriginEdges.Where(e => !e.Dest.IsInfected);
            double closestDistance = influenceEdges.Min(e => e.Org.PointDist2D(e.Dest));
            p.Z += influenceEdges.Average(e => (e.Dest.Z - p.Z) / e.Dest.PointDist2D(p) * closestDistance);
        }

        public void TernariPointApproximator(Point3D p)
        {
            IEnumerable<Edge> influenceEdges = p.OriginEdges.Where(e => !e.Dest.IsInfected);
            double left = influenceEdges.Min(e => e.Dest.Z);
            double right = influenceEdges.Max(e => e.Dest.Z);
            while (right - left > Epsilon)
            {
                double thirdPart = (right - left) / 3;
                double lm = left + thirdPart;
                double rm = right - thirdPart;
                p.Z = lm;
                double leftCurvature = Subdivision.CountGaussianCurvature(p);
                p.Z = rm;
                double rightCurvature = Subdivision.CountGaussianCurvature(p);
                if (rightCurvature > leftCurvature)
                {
                    right = rm;
                }
                else
                {
                    left = lm;
                }
            }
            //Console.WriteLine("approximated point");
            p.Z = right;
        }
    }
}
