﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDataGenerator
{
    class Mainprg
    {
        static void Main(string[] args)
        {
            if (args.Length != 5)
            {
                Console.WriteLine("(filename) (points count) (maxX) (maxY) (maxZ)");
                return;
            }

            string filename = args[0];
            int pointsNumber = int.Parse(args[1]);
            int maxX = int.Parse(args[2]);
            int maxY = int.Parse(args[3]);
            int maxZ = int.Parse(args[4]);
            HashSet<Tuple<int, int, int>> points = new HashSet<Tuple<int, int, int>>();
            int count = 0;
            Random rand = new Random();
            for (int i = 0; i < pointsNumber; i++)
            {
                int x = rand.Next(maxX);
                int y = rand.Next(maxY);
                int z = rand.Next(maxZ);
                Tuple<int, int, int> point = new Tuple<int, int, int>(x, y, z);
                if (points.Contains(point))
                {
                    i--;
                    continue;
                }
                else
                    points.Add(point);
            }
            using (StreamWriter sw = new StreamWriter(filename))
            {
                sw.WriteLine(pointsNumber);
                foreach(var p in points)
                {
                    sw.WriteLine($"{p.Item1} {p.Item2} {p.Item3}");
                }
                sw.WriteLine("0\n0");
            }
        }
    }
}
